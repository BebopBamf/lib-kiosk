module LibKiosk.App (
    AppM,
    Env (..),
    Options (..),
) where

import RIO
import RIO.Process

-- | Command line arguments
data Options = Options
    { optionsVerbose :: !Bool
    }

data Env = Env
    { envLogFunc :: !LogFunc
    , envProcessContext :: !ProcessContext
    , envOptions :: !Options
    -- Add other app-specific configuration information here
    }

type AppM = RIO Env

instance HasLogFunc Env where
    logFuncL = lens envLogFunc (\x y -> x{envLogFunc = y})

instance HasProcessContext Env where
    processContextL = lens envProcessContext (\x y -> x{envProcessContext = y})
