{-# LANGUAGE CPP #-}
{-# LANGUAGE OverloadedLabels #-}

module LibKiosk.Controller where

import Data.GI.Base (new, unsafeCastTo)
import qualified GI.Gtk as Gtk
import System.Exit (die)

-- We import GI.GObject so GHC can see that GObject.GObject (returned
-- from builderGetObject) is a ManagedPtr.

import qualified GI.GObject as GObject
import RIO

-- btnClickEvent :: Gtk.Builder -> Text -> IO () -> IO ()
-- btnClickEvent builder name handler = getBuilderObj

loadHomeWindow :: IO Gtk.ApplicationWindow
loadHomeWindow = do
  builder <- new Gtk.Builder []

  #addFromFile builder "ui/home.ui"
  mWindow <- #getObject builder "window"

  maybe showError (unsafeCastTo Gtk.ApplicationWindow) mWindow
 where
  showError =
    die
      ( "Something seems to have gone wrong loading the window.\n"
          <> "Ending the example..."
      )

homePage :: Gtk.Application -> IO ()
homePage app = do
  window <- loadHomeWindow

  #addWindow app window
  #showAll window
