{-# LANGUAGE OverloadedLabels #-}

module LibKiosk.Controller.MainMenu where

import qualified GI.Gtk as Gtk
import RIO

loadMainMenuWindow :: IO Gtk.ApplicationWindow
loadMainMenuWindow = do
    builder <- new Gtk.Builder []

    #addFromFile builder "ui/main-menu.ui"
    mWindow <- #getObject builder "window"

    maybe showError (unsafeCastTo Gtk.ApplicationWindow) mWindow
  where
    showError =
        die
            ( "Something seems to have gone wrong loading the window.\n"
                <> "Ending the example..."
            )

-- btnClickEvent :: Gtk.Builder -> Text -> IO () -> IO ()
-- btnClickEvent builder name handler = getBuilderObj

homePage :: Gtk.Application -> IO ()
homePage app = do
    window <- loadHomeWindow

    #addWindow app window
    #showAll window
