{-# LANGUAGE CPP #-}
{-# LANGUAGE OverloadedLabels #-}

module LibKiosk (
  module RIO,
  module LibKiosk.App,
  runApp,
) where

import LibKiosk.App
import LibKiosk.Controller
import RIO hiding (on)

import Data.GI.Base (AttrOp ((:=)), new, on)
import qualified GI.Gtk as Gtk

runApp :: AppM ()
runApp =
  do
    logInfo "We're inside the application!"
    app <- new Gtk.Application [#applicationId := "dev.effectfree.LibKiosk"]

    on app #activate (homePage app)
    #run app Nothing

    return ()
