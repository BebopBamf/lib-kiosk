# lib-kiosk

## Execute

* Run `stack exec -- lib-kiosk-exe` to see "We're inside the application!"
* With `stack exec -- lib-kiosk-exe --verbose` you will see the same message, with more logging.

## Run tests

`stack test`
