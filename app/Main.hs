{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Main (main) where

import LibKiosk
import Options.Applicative.Simple
import qualified Paths_lib_kiosk
import RIO.Process

-- import Run

main :: IO ()
main = do
  (options, ()) <-
    simpleOptions
      $(simpleVersion Paths_lib_kiosk.version)
      "Library Kiosk App"
      "This is a haskell implementation of a old apps programming assignment"
      ( Options
          <$> switch
            ( long "verbose"
                <> short 'v'
                <> help "Verbose output?"
            )
      )
      empty
  lo <- logOptionsHandle stderr (optionsVerbose options)
  pc <- mkDefaultProcessContext
  withLogFunc lo $ \lf ->
    let app =
          Env
            { envLogFunc = lf
            , envProcessContext = pc
            , envOptions = options
            }
     in runRIO app runApp
